﻿using Simulations.LibAbstraite.GestionPopulation;
using System.Collections.Generic;

namespace Simulations.LibMetier.FourmilliereMetier.GestionPopulation
{
    class PopulationFourmis : PopulationAbstrait
    {

        List<PopulationAbstrait> listPopulation;
        public PopulationFourmis(string nom):base(nom)
        {
            listPopulation = new List<PopulationAbstrait>();
        }
       

        public override void ajouterPopulation(PopulationAbstrait population)
        {
            listPopulation.Add(population);
        }

        public override void supprimerPopulation(PopulationAbstrait population)
        {
            listPopulation.Remove(population);
        }

        public override List<PopulationAbstrait> ListPopulation()
        {
            return listPopulation;
        }
    }
}
