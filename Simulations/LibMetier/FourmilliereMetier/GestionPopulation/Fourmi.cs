﻿using Simulations.LibAbstraite.GestionPopulation;
using System;

namespace Simulations.LibMetier.FourmilliereMetier.Population
{
    public class Fourmi : PopulationAbstrait
    {
       

        public enum EnumRoles : byte { OUVRIERE, FECONDATION, SOLDAT, REINE}
        public enum EnumSexes : byte { M, F};
        public EnumRoles Role { get; set; }
        public EnumSexes Sexe { get; set; }

        public Fourmi(string nom) : base(nom)
        {

        }

        public override string ToString()
        {
            return "Fourmi " + Nom;
        }
    }
}
