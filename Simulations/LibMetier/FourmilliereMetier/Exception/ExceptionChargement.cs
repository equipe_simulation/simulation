﻿using Simulations.LibAbstraite.ExceptionAbstrait;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulations.LibMetier.FourmilliereMetier.Exception
{
    public class ExceptionChargement : ExceptionSimulation
    {
        public ExceptionChargement(Object simulation) : base(simulation)
        {
            Console.WriteLine("Problème de chargement");
        }

        public ExceptionChargement(Object simulation, string message) : base(simulation, message)
        {
            Console.WriteLine("Problème de chargement");
        }
    }
}
