﻿using Simulations.LibAbstraite.Fabriques;
using Simulations.LibAbstraite.GestionEnvironnement;
using Simulations.LibAbstraite.GestionObjets;
using Simulations.LibAbstraite.GestionPopulation;
using Simulations.LibMetier.FourmilliereMetier.GestionEnvironnement;
using Simulations.LibMetier.FourmilliereMetier.GestionPopulation;
using Simulations.LibMetier.FourmilliereMetier.GestionObjets;
using System;
using Simulations.LibMetier.FourmilliereMetier.Population;

namespace Simulations.LibMetier.FourmilliereMetier.Fabriques
{
    public class FabriqueFourmiliere : FabriqueFourmilliereAbstraite
    {
        public override string Titre { get { return "Fourmilière"; } }

        private static FabriqueFourmiliere fabriqueFourmilliere = null;

        private FabriqueFourmiliere() { }


        /********************************Début FabriqueAbstraite********************************/

        public static FabriqueFourmilliereAbstraite SingletonInstance()
        {
            if(fabriqueFourmilliere == null)
            {
                return new FabriqueFourmiliere();
            }
            return fabriqueFourmilliere;
        }

        public override EnvironnementAbstrait CreerEnvironment()
        {
            return new EnvironnementFourmilliere();
        }

        public override TerrainAbstrait CreerTerrain(string nom)
        {
            return new Zone(nom);
        }
        public override BoutDeTerrain CreerBoutDeterrain(string nom)
        {
            return new BoutDeTerrain(nom);
        }
        public override PopulationAbstrait CreerPopulation(string nom)
        {
            return new PopulationFourmis(nom);
        }

        public override Fourmi creerFourmi(string nom)
        {
            return new Fourmi(nom);
        }

        public override ObjetAbstrait CreerObjet(string nom)
        {
            switch (nom)
            {
                case "nourriture":
                    return new Nourriture();
                    break;
                case "oeuf":
                    return new Oeuf();
                    break;
                case "phéromone":
                    return new Pheromone();
                    break;
            }
            return null;
        }


        public override AccesAbstrait CreerAcces(TerrainAbstrait tdebut, TerrainAbstrait tfin)
        {
            return new Chemin(tdebut, tfin);
        }

        

        public override FourmilliereAbstraite CreerFourmilliere(string nom)
        {
            return new Fourmilliere(nom);
        }
        //****************************fin FabriqueAbstraite******************************//





    }
}
