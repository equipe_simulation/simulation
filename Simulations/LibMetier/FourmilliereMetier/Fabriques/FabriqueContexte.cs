﻿

using System;
using Simulations.LibAbstraite.EtatsAbstrait;
using Simulations.LibAbstraite.Fabriques;
using Simulations.LibMetier.Etat;

namespace Simulations.LibMetier.FourmilliereMetier.Fabriques
{
    class FabriqueContexte : FabriqueContexteAbstraite
    {
        private static FabriqueContexte fabriqueContexte = null;
        
        private FabriqueContexte() { }

        public static FabriqueContexteAbstraite SingletonInstance()
        {
            if (fabriqueContexte == null)
            {
                return new FabriqueContexte();
            }
            return fabriqueContexte;
        }

        public override EtatAbstrait CreerEtatMarche()
        {
            return  new EtatMarche();
        }

        public override EtatAbstrait CreerEtatPause()
        {
            return new EtatPause();
        }

        public override EtatAbstrait CreerEtatPrincipal()
        {
            return new EtatPrincipal();
        }

        public override EtatAbstrait CreerEtatSauvegarde()
        {
            return new EtatSauvegarde();
        }
    }
}
