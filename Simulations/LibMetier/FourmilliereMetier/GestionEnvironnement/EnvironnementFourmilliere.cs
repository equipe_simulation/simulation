﻿using Simulations.LibAbstraite.GestionPopulation;
using System.Text;
using System;
using Simulations.LibAbstraite.GestionEnvironnement;
using Simulations.LibAbstraite.Fabriques;
using Simulations.LibAbstraite.GestionObjets;
using Simulations.LibMetier.FourmilliereMetier.Fabriques;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Linq;
using System.Xml;
using Simulations.LibMetier.FourmilliereMetier.GestionPopulation;
using static Simulations.LibMetier.FourmilliereMetier.Population.Fourmi;
using Simulations.LibMetier.FourmilliereMetier.Population;

namespace Simulations.LibMetier.FourmilliereMetier.GestionEnvironnement
{
    [Serializable]
    public class EnvironnementFourmilliere : EnvironnementAbstrait
    {
        private FabriqueFourmilliereAbstraite fab;
        public FourmilliereAbstraite Fourmill { get; set; }

        public EnvironnementFourmilliere()
        {
            fab = FabriqueFourmiliere.SingletonInstance();
        }

        public override async Task ChargerEnvironnement()
        {
            IFabrique fabrique = FabriqueFourmiliere.SingletonInstance();
            Progress<int> progressBar = new Progress<int>();
            
            await Task.FromResult<bool> (lireDonneesXmlAsync("../../../Ressources/DonneesFourmilliere/xml/fourmillières.xml", progressBar, fabrique));

            for (int i = 0; i < 2; i++)
            {
                ObjetsList.Add(fabrique.CreerObjet("nomObjet" + i));
            }

            /* ZoneAbstraite b2 = fabrique.CreerZone("b2");
             ZoneAbstraite b3 = fabrique.CreerZone("b3");
             ZoneAbstraite b4 = fabrique.CreerZone("b4");
             ZoneAbstraite b5 = fabrique.CreerZone("b5");
             AjouteZoneAbstraits(b1, b2, b3, b4, b5);
             */
            /*            AccesAbstrait ch1 = fabrique.CreerAcces(b1, b2);
                        AccesAbstrait ch2 = fabrique.CreerAcces(b2, b3);
                        AccesAbstrait ch3 = fabrique.CreerAcces(b3, b4);
                        AccesAbstrait ch4 = fabrique.CreerAcces(b3, b5);
                        AccesAbstrait ch5 = fabrique.CreerAcces(b1, b5);
                        AccesAbstrait ch6 = fabrique.CreerAcces(b2, b4);
                        AjouteChemins(fabrique, ch1, ch2, ch3, ch4, ch5, ch6);
                        */
        }



        private bool lireDonneesXmlAsync(string path, IProgress<int> progressBar, IFabrique fabrique)
        {
      
            Terrain = fabrique.CreerTerrain("Environnement fourmilliere");
            Population = fabrique.CreerPopulation("nomPopulation");
            ObjetsList = new List<ObjetAbstrait>();
            Fourmill = fab.CreerFourmilliere("Fourmilliere");
            XmlTextReader reader = new XmlTextReader(path);
            int nbNoeudTotalFourmillieres = 0;
            int nbNoeudTotalPersonnage = 0;
            int nbNoeudTotalObjet = 0;
            int nbNoeudTotalFourmilliere = 0;
            
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        switch (reader.Name)
                        {
                            case "fourmillieres" :
                                if (reader.HasAttributes)
                                {
                                    nbNoeudTotalFourmillieres = Int32.Parse(reader.GetAttribute("nombreNoeud"));
                                }
                                break;

                            case "hauteur":
                                chargerTerrain(reader);                        
                                break;

                            case "fourmilliere":
                                if (reader.HasAttributes)
                                {
                                    nbNoeudTotalFourmilliere = Int32.Parse(reader.GetAttribute("nombreNoeud"));
                                }
                                break;

                            case "limite":

                                chargerFourmilliere(reader);
                                break;

                            case "personnages":
                                nbNoeudTotalPersonnage =chargerPopulation(reader);
                                break;

                            case "objets":
                                if (reader.HasAttributes)
                                {
                                    nbNoeudTotalObjet = Int32.Parse(reader.GetAttribute("nombreNoeud"));
                                }
                                break;
                        }

                        Console.WriteLine(reader.Name);
                        break;
                    case XmlNodeType.Text: //Display the text in each element.
                        Console.WriteLine(reader.Value);
                        break;
                    case XmlNodeType.EndElement: //Display the end of the element.
                        Console.Write("</" + reader.Name);
                        Console.WriteLine(">");
                        break;
                }
            }
            // XElement element= await Task.FromResult<XElement>(XElement.Load(path));

            return true;
        }

        private void chargerTerrain(XmlTextReader reader)
        {
            reader.Read();
            int hauteur = Int32.Parse(reader.Value);
            while (reader.Name != "largeur")
            {
                reader.Read();
            }

            reader.Read();
            int largeur = Int32.Parse(reader.Value);
            int nbBoutDeTerrain = hauteur * largeur;
            for (int i = 0; i < nbBoutDeTerrain; i++)
            {
                Terrain.ajouterBoutDeTerrain(fab.CreerBoutDeterrain("Bout de terrain N°" + i));
            }
        }

        private void chargerFourmilliere(XmlTextReader reader)
        {
            Fourmill.Terrain = fab.CreerTerrain("Zone");
            do
            {
                while (reader.Name != "zone" || reader.NodeType == XmlNodeType.EndElement)
                {
                    if (reader.Name == "limite" && reader.NodeType == XmlNodeType.EndElement)
                    {
                        break;
                    }
                    reader.Read();
                }
                if (reader.Name == "limite" && reader.NodeType == XmlNodeType.EndElement)
                {
                    break;
                }
                reader.Read();

                int pos = Int32.Parse(reader.Value);
                List<TerrainAbstrait> t = Terrain.ListBoutDeTerrain();

                Fourmill.Terrain.ajouterBoutDeTerrain(t[pos]);
            } while (reader.NodeType != XmlNodeType.EndElement && reader.Name != "limite");
        }


        private int chargerPopulation(XmlTextReader reader)
        {
            int nbNoeudTotalPersonnage = 0;
            //pour le progressBar
            if (reader.HasAttributes)
            {
                nbNoeudTotalPersonnage = Int32.Parse(reader.GetAttribute("nombreNoeud"));
            }
            int cptFourmi = 0;
            //lecture de la balise personnage: Elle ccontient plusieurs balises fourmi
            do
            {
                //lecture d'une balise fourmi: Elle contient les balises role, sexe et zones
                do
                {
                    reader.Read();
                } while ((reader.Name != "fourmi" || reader.NodeType == XmlNodeType.EndElement) && (reader.Name != "personnages" || reader.NodeType != XmlNodeType.EndElement));
                if (reader.HasAttributes)
                {
                    //le nombre de fourmi(ayant le role cité dans cette balise fourmi) à ajouter dans l'environnement fourmilliere
                    int nbFourmi = Int32.Parse(reader.GetAttribute("nombre"));
                    //lecture du role
                    do
                    {
                        reader.Read();
                    } while ((reader.Name != "role" || reader.NodeType == XmlNodeType.EndElement) && (reader.Name != "personnage" || reader.NodeType != XmlNodeType.EndElement));
                    EnumRoles role;
                    switch (reader.Value)
                    {
                        case "ouvriere":
                            role = EnumRoles.OUVRIERE;
                            break;
                        case "fécondation":
                            role = EnumRoles.FECONDATION;
                            break;
                        case "soladat":
                            role = EnumRoles.SOLDAT;
                            break;
                        case "reine":
                            role = EnumRoles.REINE;
                            break;
                        default:
                            role = EnumRoles.OUVRIERE;
                            break;

                    }
                    //lecture du sexe
                    do
                    {
                        reader.Read();
                    } while ((reader.Name != "sexe" || reader.NodeType == XmlNodeType.EndElement) && (reader.Name != "personnages" || reader.NodeType != XmlNodeType.EndElement));
                    EnumSexes sexe = (reader.Value == "M")? EnumSexes.M : EnumSexes.F;
                    //lecture de la balise zones
                    do
                    {
                        //lecture d'une zone
                        do
                        {
                            reader.Read();
                        } while ((reader.Name != "zone" || reader.NodeType == XmlNodeType.EndElement) && (reader.Name != "zones" || reader.NodeType != XmlNodeType.EndElement));
                        if(reader.Name == "zones" && reader.NodeType == XmlNodeType.EndElement)
                        {
                            break;
                        }
                        reader.Read();
                        int pos = Int32.Parse(reader.Value);
                        List<TerrainAbstrait> t = Terrain.ListBoutDeTerrain();
                        Fourmi fourm = fab.creerFourmi("Fourmi N°" + cptFourmi);
                        cptFourmi++;
                        fourm.Terrain = fab.CreerTerrain("Terrain fourmi");
                        fourm.Terrain.ajouterBoutDeTerrain(t[pos]);
                        fourm.Role = role;
                        fourm.Sexe = sexe;
                        //ajout  de la posittion de la fourmi sur la map (environnement fourmilliere)
                        Population.ajouterPopulation(fourm);
                    } while ((reader.Name != "zones" || reader.NodeType == XmlNodeType.EndElement) && (reader.Name != "personnage" || reader.NodeType != XmlNodeType.EndElement));
                   
                }


            } while (reader.Name != "personnages" || reader.NodeType != XmlNodeType.EndElement);
            return nbNoeudTotalPersonnage;
        }

        private void lireDonneeBinaire(string binaire)
        {

        }







        public override string Simuler()
        {
            StringBuilder sb = new StringBuilder();
            foreach (PopulationAbstrait unPersonnage in Population.ListPopulation())
            {
               // ZoneAbstraite zoneAbstraiteSource = unPersonnage.Position;

              /*  var accesList = zoneAbstraiteSource.AccesAbstraitList;
                if (accesList.Count > 0)
                {
                    ZoneAbstraite zoneAbstraiteCible = unPersonnage.ChoixZoneSuivante(accesList);

                    DeplacerPersonnage(unPersonnage, zoneAbstraiteSource, zoneAbstraiteCible);
                    sb.AppendFormat("{0} : {1} --> {2}\n", unPersonnage.Nom, zoneAbstraiteSource.Nom,
                        zoneAbstraiteCible.Nom);
                }
                else
                {
                    sb.AppendFormat("{0} : dans cul de sac\n", unPersonnage.Nom);
                }*/
            }
            return sb.ToString();
        }


        public override string Statistiques()
        {
            throw new NotImplementedException();
        }

      /*  private void DeplacerPersonnage(PopulationAbstrait unPersonnage,
            ZoneAbstraite zoneSource, ZoneAbstraite zoneCible)
        {
            unPersonnage.Position = zoneCible;
            //zoneSource.RetirePersonnage(unPersonnage);
            //zoneCible.AjoutePersonnage(unPersonnage);
        }*/

        public override void notifier()
        {
            Console.WriteLine("notification");
        }
    }
}
