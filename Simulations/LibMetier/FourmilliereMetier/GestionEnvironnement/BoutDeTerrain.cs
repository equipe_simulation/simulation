﻿using System;
using System.Collections.Generic;
using Simulations.LibAbstraite.GestionEnvironnement;
using Simulations.LibAbstraite.GestionPopulation;
using Simulations.LibAbstraite.GestionObjets;

namespace Simulations.LibMetier.FourmilliereMetier.GestionEnvironnement
{
    public class BoutDeTerrain : TerrainAbstrait
    {
        public List<AccesAbstrait> AccesAbstraitList { get; set; }

        public List<PopulationAbstrait> ListFourmi { get; set; }

        public List<ObjetAbstrait> ObjetsList { get; set; }

        public BoutDeTerrain(string unNom) : base(unNom)
        {
            AccesAbstraitList = new List<AccesAbstrait>();
            ListFourmi = new List<PopulationAbstrait>();
            ObjetsList = new List<ObjetAbstrait>();
        }

        public override void AjouteAcces(AccesAbstrait acces)
        {
            AccesAbstraitList.Add(acces);
        }

        public void AjoutePersonnage(PopulationAbstrait unPersonnage)
        {
            ListFourmi.Add(unPersonnage);
        }

        public void RetirePersonnage(PopulationAbstrait unPersonnage)
        {
            ListFourmi.Remove(unPersonnage);
        }



        public void AjouteObjet(ObjetAbstrait unObjet)
        {
            ObjetsList.Add(unObjet);
        }

        public override void construire()
        {
            throw new NotImplementedException();
        }

    }
}
