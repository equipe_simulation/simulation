﻿using System;
using Simulations.LibAbstraite.GestionEnvironnement;
using System.Collections.Generic;

namespace Simulations.LibMetier.FourmilliereMetier.GestionEnvironnement
{
    public class Zone : TerrainAbstrait
    {
        private List<TerrainAbstrait> listBoutDeTerrain;

        public Zone(string unNom) : base(unNom)
        {
            listBoutDeTerrain = new List<TerrainAbstrait>();
        }

        public override void construire()
        {
            throw new NotImplementedException();
        }

        public override void ajouterBoutDeTerrain(TerrainAbstrait unBoutDeTerrain)
        {
            listBoutDeTerrain.Add(unBoutDeTerrain);
        }

        public override void supprimerBoutDeTerrain(TerrainAbstrait unBoutDeTerrain)
        {
            listBoutDeTerrain.Remove(unBoutDeTerrain);
        }

        public override List<TerrainAbstrait> ListBoutDeTerrain()
        {
            return listBoutDeTerrain;
        }

       
    }
}
