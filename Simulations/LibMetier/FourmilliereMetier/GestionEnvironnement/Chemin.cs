﻿using Simulations.LibAbstraite.GestionEnvironnement;

namespace Simulations.LibMetier.FourmilliereMetier.GestionEnvironnement
{
    public class Chemin : AccesAbstrait
    {
        public Chemin(TerrainAbstrait debut,  TerrainAbstrait fin) : base(debut, fin)
        {
        }
    }
}
