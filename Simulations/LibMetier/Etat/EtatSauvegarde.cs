﻿

using Simulations.LibAbstraite.EtatsAbstrait;
using Simulations.LibAbstraite.Fabriques;

namespace Simulations.LibMetier.Etat
{
    class EtatSauvegarde : EtatAbstrait
    {
        public override void ModifieEtat(ContexteAbstrait unContexte, FabriqueContexteAbstraite fabrique)
        {
            EnumEtatAction action = gestionEtat(unContexte);
            switch (action)
            {
                case EnumEtatAction.Principal:
                    unContexte.EtatCourant = fabrique.CreerEtatPrincipal();
                    break;
                case EnumEtatAction.Pause:
                    unContexte.EtatCourant = fabrique.CreerEtatPause();
                    break;
                case EnumEtatAction.Marche:
                    unContexte.EtatCourant = fabrique.CreerEtatMarche();
                    break;
            }
        }
        private EnumEtatAction gestionEtat(ContexteAbstrait contexte)
        {
            EnumEtatAction action = EnumEtatAction.Marche;
            return action;
        }
    }
}
