﻿using Simulations.LibAbstraite.GestionEnvironnement;
using Simulations.LibAbstraite.EtatsAbstrait;
using System.Collections.Generic;
using System;
using Simulations.LibAbstraite.Fabriques;
using Simulations.LibMetier.FourmilliereMetier.Fabriques;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Simulations.LibMetier.Etat
{
    public class Contexte : ContexteAbstrait
    {
        public override EtatAbstrait EtatCourant { get; set; }
        public override EnvironnementAbstrait EnvironnementCourant { get; set; }
        public override List<EnvironnementAbstrait> listEnvironnement { get; set; }
        private List<IFabrique> ListFabrique;
        private FabriqueContexteAbstraite fabriqueContexte;

        public Contexte()
        {
            listEnvironnement = new List<EnvironnementAbstrait>();
            ListFabrique = new List<IFabrique>();
            fabriqueContexte = FabriqueContexte.SingletonInstance();   
            //EnvironnementCourant = fabrique.CreerEnvironment();
            //listEnvironnement.Add()
        }
        public override void Charger()
        {
            ListFabrique.Add(FabriqueFourmiliere.SingletonInstance());
            foreach (IFabrique fabrique in ListFabrique)
            {
                listEnvironnement.Add(fabrique.CreerEnvironment());
            }
            EtatCourant = fabriqueContexte.CreerEtatPrincipal();
        }
        public override void Execute()
        {
            EtatCourant.ModifieEtat(this, fabriqueContexte);
        }

        private static void Sauvegarder(object toSave, string path)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            FileStream flux = null;
            try
            {

                flux = new FileStream(path, FileMode.Create, FileAccess.Write);

                formatter.Serialize(flux, toSave);

                flux.Flush();
            }
            catch { }
            finally
            {

                if (flux != null)
                    flux.Close();
            }
        }
    }
}
