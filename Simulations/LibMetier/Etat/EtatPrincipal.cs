﻿
using Simulations.LibAbstraite.EtatsAbstrait;
using Simulations.LibAbstraite.Fabriques;
using Simulations.LibAbstraite.GestionEnvironnement;
using Simulations.LibMetier.FourmilliereMetier.Fabriques;
using System.Threading.Tasks;

namespace Simulations.LibMetier.Etat
{
    public class EtatPrincipal : EtatAbstrait
    {
        public override  void ModifieEtat(ContexteAbstrait unContexte, FabriqueContexteAbstraite fabrique)
        {
            Task<EnumEtatAction> action =  gestionEtat(unContexte);

            switch (action.Result)
            {
                case EnumEtatAction.Marche:
                    unContexte.EtatCourant = fabrique.CreerEtatMarche();
                    break;
                case EnumEtatAction.Pause:
                    unContexte.EtatCourant = fabrique.CreerEtatPause();
                    break;
                case EnumEtatAction.Sauvegarde:
                    unContexte.EtatCourant = fabrique.CreerEtatSauvegarde();
                    break;
            }
        }
        private async Task<EnumEtatAction> gestionEtat(ContexteAbstrait contexte)
        {
            foreach (EnvironnementAbstrait environnement in contexte.listEnvironnement)
            {
                await environnement.ChargerEnvironnement();
            }

            EnumEtatAction action = EnumEtatAction.Marche;
            return action;
        }
    }
}
