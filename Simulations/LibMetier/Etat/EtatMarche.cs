﻿using Simulations.LibAbstraite.EtatsAbstrait;
using Simulations.LibAbstraite.Fabriques;
using Simulations.LibAbstraite.GestionEnvironnement;

namespace Simulations.LibMetier.Etat
{
    public class EtatMarche : EtatAbstrait
    {
        public override void ModifieEtat(ContexteAbstrait unContexte, FabriqueContexteAbstraite fabrique)
        {
            EnumEtatAction action = gestionEtat(unContexte);
            switch (action)
            {
                case EnumEtatAction.Principal:
                    unContexte.EtatCourant = fabrique.CreerEtatPrincipal();
                    break;
                case EnumEtatAction.Pause:
                    unContexte.EtatCourant = fabrique.CreerEtatPause();
                    break;
               case EnumEtatAction.Sauvegarde:
                    unContexte.EtatCourant = fabrique.CreerEtatSauvegarde();
                    break;
            }
        }
        private EnumEtatAction gestionEtat(ContexteAbstrait contexte)
        {
            foreach (EnvironnementAbstrait environnement in contexte.listEnvironnement)
            {
                environnement.Simuler();
            }
            EnumEtatAction action= EnumEtatAction.Marche;
            return action;
        }
    }
}
