﻿using Simulations.LibAbstraite.GestionObjets;
using Simulations.LibAbstraite.GestionPopulation;
using Simulations.LibAbstraite.EtatsAbstrait;
using Simulations.LibAbstraite.GestionEnvironnement;
using System.Threading.Tasks;
using Simulations.LibMetier.FourmilliereMetier.Population;
using Simulations.LibMetier.FourmilliereMetier.GestionEnvironnement;

namespace Simulations.LibAbstraite.Fabriques
{
    public abstract class FabriqueFourmilliereAbstraite : IFabrique
    {
        public abstract string Titre { get; }

        /**********************************Début IFabrique******************************/


        public abstract EnvironnementAbstrait CreerEnvironment();

        public abstract TerrainAbstrait CreerTerrain(string nom);
        
        public abstract PopulationAbstrait CreerPopulation(string nom);
       
        public abstract ObjetAbstrait CreerObjet(string nom);



        //*********************************Fin IFabrique*******************************//
        public abstract BoutDeTerrain CreerBoutDeterrain(string nom);

        public abstract Fourmi creerFourmi(string nom);

        public abstract AccesAbstrait CreerAcces(TerrainAbstrait tdebut, TerrainAbstrait tfin);
        

        public abstract FourmilliereAbstraite CreerFourmilliere(string nom);

    }
}
