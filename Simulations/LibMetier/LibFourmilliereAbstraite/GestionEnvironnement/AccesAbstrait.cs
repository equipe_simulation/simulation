﻿namespace Simulations.LibAbstraite.GestionEnvironnement
{
    public abstract class AccesAbstrait
    {
        public TerrainAbstrait Debut { get; set; }
        public TerrainAbstrait Fin { get; set; }

        protected AccesAbstrait(TerrainAbstrait debut, TerrainAbstrait fin)
        {
            Debut = debut;
            Debut.AjouteAcces(this);
            Fin = fin;
        }
    }
}
