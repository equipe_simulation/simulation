﻿using Simulations.LibAbstraite.GestionPopulation;
using Simulations.LibMetier.FourmilliereMetier.GestionEnvironnement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulations.LibAbstraite.GestionEnvironnement
{
    public abstract class FourmilliereAbstraite
    {
        public string Nom { get; set; }
        public PopulationAbstrait Population { get; set; }
        //c'est la position de la fourmilliere dans la map
        public TerrainAbstrait Terrain { get; set; }

        public FourmilliereAbstraite(string nom)
        {
            this.Nom = nom;
        }

    }
}
