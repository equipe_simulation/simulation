﻿using Simulations.LibAbstraite.GestionEnvironnement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulations.LibAbstraite.GestionObjets
{
    public abstract class ObjetAbstrait
    {
        public string Nom { get; set; }

      //  public ZoneAbstraite Position { get; set; }

        protected ObjetAbstrait()
        {
        //    Position = null;
        }
    }
}
