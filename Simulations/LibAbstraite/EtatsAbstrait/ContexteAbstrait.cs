﻿

using Simulations.LibAbstraite.GestionEnvironnement;
using System.Collections.Generic;

namespace Simulations.LibAbstraite.EtatsAbstrait
{
    public abstract class ContexteAbstrait
    {
        public abstract EtatAbstrait EtatCourant { get; set; }
        public abstract EnvironnementAbstrait EnvironnementCourant { get; set; }
        public abstract List<EnvironnementAbstrait> listEnvironnement { get; set; }
        public abstract void Charger();
        public abstract void Execute();
    }
}
