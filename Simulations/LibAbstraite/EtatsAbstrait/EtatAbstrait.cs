﻿using Simulations.LibAbstraite.Fabriques;

namespace Simulations.LibAbstraite.EtatsAbstrait
{
    public abstract class EtatAbstrait
    {
        public abstract void ModifieEtat(ContexteAbstrait unContexte, FabriqueContexteAbstraite fabrique);
    }
}
