﻿using System.Collections.Generic;
using System.Xml.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Simulations.LibAbstraite.EtatsAbstrait;
using Simulations.LibAbstraite.Fabriques;
using Simulations.LibAbstraite.GestionPopulation;
using Simulations.LibAbstraite.GestionObjets;
using System.Threading.Tasks;
using System;
using System.Xml;

namespace Simulations.LibAbstraite.GestionEnvironnement
{
    [Serializable]
    public abstract class EnvironnementAbstrait
    {
        public PopulationAbstrait Population { get; set; }
        public List<ObjetAbstrait> ObjetsList { get; set; }
        public TerrainAbstrait Terrain { get; set; }
       

        private EtatAbstrait EtatCourant { get; set; }

        //----------------------------------------------------------------------
        public virtual async Task ChargerEnvironnement()
        {
           
        //    lireDonneesXml("");
          //  lireDonneesBinaire<EnvironnementAbstrait>("");

        }

        protected virtual async Task<XElement> lireDonneesXmlAsync(string path)
        {

           XmlTextReader reader = new XmlTextReader(path);
            //XElement element = new XElement("Donnees fourmilliere");
            bool isEnfant = true;
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        
                        
                        Console.WriteLine(reader.ToString());
                        break;
                    case XmlNodeType.Text: //Display the text in each element.
                        Console.WriteLine(reader.Value);
                        break;
                    case XmlNodeType.EndElement: //Display the end of the element.
                        Console.Write("</" + reader.Name);
                        Console.WriteLine(">");
                        break;
                }
            }
           // XElement element= await Task.FromResult<XElement>(XElement.Load(path));

            return new XElement(path); 
        }

        protected virtual async Task<T> lireDonneesBinaireAsync<T>(string path)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream flux = null;
            try
            {
                await Task.Factory.StartNew(delegate
                {
                    flux = new FileStream(path, FileMode.Open, FileAccess.Read);
                });
                
                return (T)formatter.Deserialize(flux);
            }
            catch
            {
                return default(T);
            }
            finally
            {
                if (flux != null)
                    flux.Close();
            }

        }

        public abstract string Simuler();



        public abstract string Statistiques();


        public abstract void notifier();   
    }
}
