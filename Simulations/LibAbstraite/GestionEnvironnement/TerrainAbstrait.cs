﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulations.LibAbstraite.GestionEnvironnement
{
    public abstract class TerrainAbstrait
    {
        private string Nom { get; set; }
        public TerrainAbstrait(string unNom)
        {
            Nom = unNom;
        }
        public abstract void construire();

        public virtual void AjouteAcces(AccesAbstrait acces)
        {
            Console.WriteLine("Je suis un terrain abstrait. On ne peut m'ajouter un accès");
        }

        public virtual void ajouterBoutDeTerrain(TerrainAbstrait unBoutDeTerrain)
        {
            Console.WriteLine("Je suis terrain abstrait. Je ne peux être composé");
        }

        public virtual void supprimerBoutDeTerrain(TerrainAbstrait unBoutDeTerrain)
        {
            Console.WriteLine("Je suis un terrain abstrait. Je ne peux être décomposé");
        }

        public virtual List<TerrainAbstrait> ListBoutDeTerrain()
        {
            Console.WriteLine("Je suis un terrain abstrait. Je ne possède pas de liste de ma composition");
            return null;
        }
    }
}
