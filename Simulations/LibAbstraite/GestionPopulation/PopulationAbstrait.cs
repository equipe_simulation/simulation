﻿using System;
using System.Collections.Generic;
using Simulations.LibAbstraite.GestionEnvironnement;

namespace Simulations.LibAbstraite.GestionPopulation
{
    public abstract class PopulationAbstrait
    {
        protected static readonly Random Hasard = new Random();
        public string Nom { get; set; }
        public TerrainAbstrait Terrain { get; set; }

      //  public ZoneAbstraite Position { get; set; }

        protected PopulationAbstrait(string nom)
        {
            this.Nom =nom;
        }

        public virtual void ajouterPopulation(PopulationAbstrait population)
        {
            Console.WriteLine("Je suis une population abstraite. Je ne peux être composé");
        }
        public virtual void supprimerPopulation(PopulationAbstrait population)
        {
            Console.WriteLine("Je suis une population abstraite. Je ne peux être décomposé");
        }

      /*  public virtual ZoneAbstraite ChoixZoneSuivante(List<AccesAbstrait> accesList)
        {
            int x = Hasard.Next(accesList.Count);
            return accesList[x].Fin;
        }*/

        public virtual List<PopulationAbstrait> ListPopulation()
        {
            Console.WriteLine("Je suis une population abstraite. Je ne possède pas de liste de ma composition");
            return null;
        }

    }
}
