﻿using System;

namespace Simulations.LibAbstraite.ExceptionAbstrait
{
    public class ExceptionSimulation : System.Exception
    {
        protected ExceptionSimulation(Object simulation) : base()  {
            Console.WriteLine("Exception dans la simulation " + simulation.GetType().ToString());
        }

        protected ExceptionSimulation(Object simulation, string message) : base(message)
        {
            Console.WriteLine(base.StackTrace + "/ Exception dans la simulation " + simulation.GetType().ToString());
        }
    }
}
