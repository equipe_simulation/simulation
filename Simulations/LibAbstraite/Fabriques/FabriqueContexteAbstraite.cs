﻿using Simulations.LibAbstraite.EtatsAbstrait;


namespace Simulations.LibAbstraite.Fabriques
{
    public abstract class FabriqueContexteAbstraite
    {
        public abstract EtatAbstrait CreerEtatMarche();
        public abstract EtatAbstrait CreerEtatPrincipal();
        public abstract EtatAbstrait CreerEtatSauvegarde();
        public abstract EtatAbstrait CreerEtatPause();
    }
}
