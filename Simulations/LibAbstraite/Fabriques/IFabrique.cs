﻿using Simulations.LibAbstraite.GestionEnvironnement;
using Simulations.LibAbstraite.GestionObjets;
using Simulations.LibAbstraite.GestionPopulation;
using Simulations.LibAbstraite.EtatsAbstrait;
using System.Threading.Tasks;

namespace Simulations.LibAbstraite.Fabriques
{
    public interface IFabrique
    {
        

       EnvironnementAbstrait CreerEnvironment();

       PopulationAbstrait CreerPopulation(string nom);

        ObjetAbstrait CreerObjet(string nom);

        TerrainAbstrait CreerTerrain(string nom);
    }

}
