﻿using Simulations.LibAbstraite.EtatsAbstrait;
using Simulations.LibMetier.Etat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InterfaceGraphique
{
    /// <summary>
    /// Logique d'interaction pour Observable.xaml
    /// </summary>
    public partial class Observable : UserControl
    {
        private ContexteAbstrait ContextEnvironnement { get; set; }
        public Observable(ContexteAbstrait contextEnvironnement)
        {
            this.ContextEnvironnement= contextEnvironnement;
            contextEnvironnement.Charger();
            contextEnvironnement.Execute();
            InitializeComponent();
        }
    }
}
