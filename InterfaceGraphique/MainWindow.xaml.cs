﻿using Simulations.LibAbstraite.EtatsAbstrait;
using Simulations.LibMetier.Etat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InterfaceGraphique
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        ContexteAbstrait contextEnvironnement;
        private BackgroundWorker bw;
        public MainWindow()
        {
            InitializeComponent();
            contextEnvironnement = new Contexte();
            bw = new BackgroundWorker();
            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ImageBrush ib = new ImageBrush();
            ib.ImageSource = (BitmapImage)FindResource("Background");
            this.Background = ib;
            MenuOption.Background = ib;
            MenuOption.Foreground = Brushes.DarkCyan;
            MenuItemPause.Background = ib;
            MenuItemCharger.Background = ib;
            MenuItemSauvegarder.Background = ib;
            MenuItemExit.Background = ib;
            btnSimulaionTour.Background = ib;
            btnSimulaionTour.Foreground = Brushes.DarkCyan;

            btnSimulaionTrafic.Background = ib;
            btnSimulaionTrafic.Foreground = Brushes.DarkCyan;

            btnSimulationFourmillière.Background = ib;
            btnSimulationFourmillière.Foreground = Brushes.DarkCyan;
            contextEnvironnement.Charger();
        }




        private void ClickBtnChargerTrafic(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("ChargerTraffic");
        }

        private void ClickBtnChargerFourmilliere(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("ChargerFoourmilliere");
            
            Selection_Simulation.Opacity = 0;
            new Observable(contextEnvironnement);

        }

        private void ClickBtnChargerTour(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("ChargerTour");
        }

        private void ClickMenuItemPause(object sender, RoutedEventArgs e)
        {

        }

        private void ClickMenuItemCharger(object sender, RoutedEventArgs e)
        {

        }

        private void ClickMenuItemSauvegarder(object sender, RoutedEventArgs e)
        {

        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            for (int i = 1; (i <= 10); i++)
            {
                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    // Perform a time consuming operation and report progress.
                    System.Threading.Thread.Sleep(500);
                    worker.ReportProgress((i * 10));
                }
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
           // this.tbProgress.Text = (e.ProgressPercentage.ToString() + "%");
        }

    }
}
